<nav class="navbar navbar-default" role="navigation">
<div class="container">
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li><a href="#buscar">BUSCAR</a></li>
    </ul>
<form class="navbar-form navbar-left" role="search" action="./buscar.php">
      <div class="form-group">
        <input type="text" name="s" class="form-control" placeholder="Buscar" id="buscar" required>
      </div>
      <button type="submit" class="btn btn-default">&nbsp;<i class="glyphicon glyphicon-search"></i>&nbsp;</button>
    </form>
  </div>
</div>
</nav>